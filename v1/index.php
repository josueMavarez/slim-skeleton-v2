<?php

require '../vendor/autoload.php';
require_once 'dbHandler.php';
require_once 'passwordHash.php';

$app = new \Slim\Slim();

$app->get('/test', function() use ($app) {
    $db = new DbHandler();
    $test = "test";
    echoResponse(200, $test);
});

function verifyRequiredParams($required_fields) {
    $error = false;
    $error_fields = "";
    $request_params = array();
    $request_params = $_REQUEST;

    if ($_SERVER['REQUEST_METHOD'] == 'PUT') {
        $app = \Slim\Slim::getInstance();
        parse_str($app->request()->getBody(), $request_params);
    }
    foreach ($required_fields as $field) {
        if (!isset($request_params[$field]) || strlen(trim($request_params[$field])) <= 0) {
            $error = true;
            $error_fields .= $field . ', ';
        }
    }

    if ($error) {
        $response = array();
        $app = \Slim\Slim::getInstance();
        $response["error"] = true;
        $response["message"] = 'Required field(s) ' . substr($error_fields, 0, -2) . ' is missing or empty' . $app->request->post('user');
        echoRespnse(400, $response);
        $app->stop();
    }
}

function echoResponse($status_code, $response) {
    $app = \Slim\Slim::getInstance();

    $app->status($status_code);

    $app->contentType('application/json');

    echo json_encode($response, JSON_PRETTY_PRINT);
}

$app->run();

?>
