<?php
    class dbConnect {

        private $conn;

        function __construct() {
        }

        function connect() {
            include_once '../dbConfig.php';
            $this->conn = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);

            if (mysqli_connect_errno()) {
                echo "Failed to connect to MySQL: " . mysqli_connect_error();
            }

            mysqli_set_charset($this->conn, "utf8");
            return $this->conn;
        }
    }
?>
